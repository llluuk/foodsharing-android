# foodsharing android app

An android app for foodsharing. Not yet released;

* written in [Kotlin](https://kotlinlang.org/)
* uses dagger/retrofit/okhttp libraries

Current features:
* account
  * login and logout
* chat
  * list of existing chats
  * individual chat with websocket updates
* map
  * shows food baskets and fair share points

It's quite basic for now still, lots of rough edges.

## Getting Started

You need to connect to a backend, there are two options:
* beta.foodsharing.de (default if you do nothing else)
* local backend (see [main repo](https://gitlab.com/foodsharing-dev/foodsharing) for details).

To use a local backend create/edit a `local.properties` file in the base directory with the contents: 
```
foodsharing.baseURL=http://your.ip.address.here:18080
```

## Commands

There are a few useful things you might run manually, or integrate into your IDE.

| command | purpose |
|---|---|
| `./gradlew ktlintCheck` | check code according to [ktlint](https://ktlint.github.io/) |
| `./gradlew ktlintFormat` | reformat code according to [ktlint](https://ktlint.github.io/) |
| `./gradlew ktlintApplyToIdea` | generate and apply ktlint rules to Intellij IDEs using [JLLeitschuh/ktlint-gradle](https://github.com/JLLeitschuh/ktlint-gradle#additional-helper-tasks) |
| `./gradlew dependencyUpdates` | check for dependency updates using [ben-manes/gradle-versions-plugin](https://github.com/ben-manes/gradle-versions-plugin) |


## IDE

We use either Android Studio or Intellij Ultimate Edition.

You might be able to get it to work in some other way, but you'll be on your own for now :) 

## Community

Come and say hi in __#fs-dev-android__ channel in 
[yunity slack](https://slackin.yunity.org/).

## Builds

You can get access to builds via GitLab artifacts for a given pipeline build.

Also published to __#fs-dev-android-git__ channel in 
[yunity slack](https://slackin.yunity.org/)
