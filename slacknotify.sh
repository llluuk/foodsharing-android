#!/bin/bash

set -e

ARTIFACTS_SUBPATH="-/jobs/$CI_JOB_ID/artifacts"
ARTIFACTS_BASE_VIEW_URL="https://$CI_PROJECT_NAMESPACE.gitlab.io/-/$CI_PROJECT_NAME/$ARTIFACTS_SUBPATH"
ARTIFACTS_BASE_DOWNLOAD_URL="https://gitlab.com/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME/$ARTIFACTS_SUBPATH/raw"

# Must fit with what happens in .gitlab-ci.yml
APK_FILENAME="app-debug.$CI_COMMIT_SHA.apk"
APK_PATH="app/build/outputs/apk/debug/$APK_FILENAME"

APK_URL="$ARTIFACTS_BASE_DOWNLOAD_URL/$APK_PATH"

LINT_REPORT_PATH="app/build/reports/lint-results-debug.html"
LINT_REPORT_URL="$ARTIFACTS_BASE_VIEW_URL/$LINT_REPORT_PATH"

if [ ! -z "$SLACK_WEBHOOK_URL" ]; then

  ATTACHMENT_TEXT=""

  ATTACHMENT_TEXT+=":android: <$APK_URL|Download the apk>"
  ATTACHMENT_TEXT+="\n:page_facing_up: <$LINT_REPORT_URL|View the lint report>"

  payload=$(printf '{
    "channel": "#fs-dev-android-git",
    "username": "build",
    "text": ":sparkles: Successful build of *foodsharing app* %s :rocket:",
    "attachments": [
      {
        "text": "%s"
      }
    ]
  }' "$CI_COMMIT_REF_NAME" "$ATTACHMENT_TEXT")

  curl -X POST --data-urlencode "payload=$payload" "$SLACK_WEBHOOK_URL"

fi