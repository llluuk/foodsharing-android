package de.foodsharing.di

import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import de.foodsharing.FoodsharingApplication

@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivitiesModule::class,
        ApplicationModule::class,
        FragmentsModule::class,
        PresentersModule::class
    ]
)
interface ApplicationComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: FoodsharingApplication): Builder

        fun build(): ApplicationComponent
    }

    fun inject(app: FoodsharingApplication)
}