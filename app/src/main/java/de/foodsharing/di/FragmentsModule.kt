package de.foodsharing.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import de.foodsharing.ui.baskets.BasketsFragment
import de.foodsharing.ui.conversations.ConversationsFragment
import de.foodsharing.ui.map.MapFragment

@Module
abstract class FragmentsModule {

    @ContributesAndroidInjector
    abstract fun bindConversationListFragment(): ConversationsFragment

    @ContributesAndroidInjector
    abstract fun bindMapFragment(): MapFragment

    @ContributesAndroidInjector
    abstract fun bindBasketListFragment(): BasketsFragment
}