package de.foodsharing.di

import com.franmontiel.persistentcookiejar.ClearableCookieJar
import dagger.Module
import dagger.Provides
import de.foodsharing.api.AuthAPI
import de.foodsharing.api.BasketAPI
import de.foodsharing.api.ConversationsAPI
import de.foodsharing.api.MapAPI
import de.foodsharing.api.WebsocketAPI
import de.foodsharing.ui.basket.BasketContract
import de.foodsharing.ui.basket.BasketPresenter
import de.foodsharing.ui.baskets.BasketsContract
import de.foodsharing.ui.baskets.BasketsPresenter
import de.foodsharing.ui.conversation.ConversationContract
import de.foodsharing.ui.conversation.ConversationPresenter
import de.foodsharing.ui.conversations.ConversationsContract
import de.foodsharing.ui.conversations.ConversationsPresenter
import de.foodsharing.ui.login.LoginContract
import de.foodsharing.ui.login.LoginPresenter
import de.foodsharing.ui.main.MainContract
import de.foodsharing.ui.main.MainPresenter
import de.foodsharing.ui.map.MapContract
import de.foodsharing.ui.map.MapPresenter
import de.foodsharing.ui.newbasket.NewBasketContract
import de.foodsharing.ui.newbasket.NewBasketPresenter

@Module
class PresentersModule {

    @Provides
    fun provideLoginPresenter(auth: AuthAPI): LoginContract.Presenter = LoginPresenter(auth)

    @Provides
    fun provideConversationsPresenter(
        conversations: ConversationsAPI,
        ws: WebsocketAPI
    ): ConversationsContract.Presenter =
        ConversationsPresenter(conversations, ws)

    @Provides
    fun provideConversationPresenter(
        conversations: ConversationsAPI,
        ws: WebsocketAPI
    ): ConversationContract.Presenter =
        ConversationPresenter(conversations, ws)

    @Provides
    fun provideMainPresenter(auth: AuthAPI, ws: WebsocketAPI, cookieJar: ClearableCookieJar): MainContract.Presenter =
        MainPresenter(auth, ws, cookieJar)

    @Provides
    fun provideMapPresenter(map: MapAPI): MapContract.Presenter = MapPresenter(map)

    @Provides
    fun provideBasketsPresenter(baskets: BasketAPI): BasketsContract.Presenter = BasketsPresenter(baskets)

    @Provides
    fun provideBasketPresenter(baskets: BasketAPI): BasketContract.Presenter = BasketPresenter(baskets)

    @Provides
    fun provideNewBasketPresenter(baskets: BasketAPI): NewBasketContract.Presenter = NewBasketPresenter(baskets)
}