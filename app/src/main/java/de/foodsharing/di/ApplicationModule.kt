package de.foodsharing.di

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.franmontiel.persistentcookiejar.ClearableCookieJar
import com.franmontiel.persistentcookiejar.PersistentCookieJar
import com.franmontiel.persistentcookiejar.cache.SetCookieCache
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import de.foodsharing.FoodsharingApplication
import de.foodsharing.api.AuthAPI
import de.foodsharing.api.BasketAPI
import de.foodsharing.api.ConversationsAPI
import de.foodsharing.api.DefaultWebsocketAPI
import de.foodsharing.api.MapAPI
import de.foodsharing.api.WebsocketAPI
import de.foodsharing.utils.BASE_URL
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.Date
import de.foodsharing.api.DateDeserializer

@Module
class ApplicationModule {

    @Provides
    fun provideApplication(app: FoodsharingApplication): Application = app

    @Provides
    fun provideContext(app: Application): Context = app

    @Provides
    fun provideSharedPreferences(app: Application): SharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(app)

    @Provides
    fun provideCookieJar(context: Context): ClearableCookieJar =
        PersistentCookieJar(SetCookieCache(), SharedPrefsCookiePersistor(context))

    @Provides
    fun provideGson(): Gson = GsonBuilder()
        .registerTypeAdapter(Date::class.java, DateDeserializer())
        .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create()

    @Provides
    fun provideHttpClient(cookieJar: ClearableCookieJar): OkHttpClient = OkHttpClient.Builder()
        .cookieJar(cookieJar)
        .addInterceptor(HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        })
        .build()

    @Provides
    fun provideRetrofit(
        gson: Gson,
        httpClient: OkHttpClient
    ): Retrofit = retrofit2.Retrofit.Builder()
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(ScalarsConverterFactory.create())
        .addConverterFactory(GsonConverterFactory.create(gson))
        .baseUrl(BASE_URL)
        .client(httpClient)
        .build()

    @Provides
    fun provideAuthAPI(retrofit: Retrofit): AuthAPI = retrofit.create(AuthAPI::class.java)

    @Provides
    fun provideConversationsAPI(retrofit: Retrofit): ConversationsAPI =
        retrofit.create(ConversationsAPI::class.java)

    @Provides
    fun provideMapAPI(retrofit: Retrofit): MapAPI = retrofit.create(MapAPI::class.java)

    @Provides
    fun provideWebsocketAPI(gson: Gson, httpClient: OkHttpClient): WebsocketAPI =
        DefaultWebsocketAPI(httpClient, gson)

    @Provides
    fun provideBasketAPI(retrofit: Retrofit): BasketAPI = retrofit.create(BasketAPI::class.java)
}