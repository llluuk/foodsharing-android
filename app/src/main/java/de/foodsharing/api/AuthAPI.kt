package de.foodsharing.api

import android.util.Log
import de.foodsharing.utils.LOG_TAG
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

/**
 * Request body for a login request.
 *
 * @see AuthAPI.login
 */
data class LoginRequest(val email: String, val password: String, val rememberMe: Boolean)

/**
 * Response of a login request.
 *
 * @see AuthAPI.login
 */
data class LoginResponse(val id: Int, val name: String)

/**
 * Retrofit API interface for user authentication.
 */
interface AuthAPI {
    /**
     * Logs in the user.
     * @param data email and password
     * @return the user's id and name
     */
    @POST("/api/user/login")
    fun login(@Body data: LoginRequest): Observable<LoginResponse>

    /**
     * Checks whether the user is logged in. The response contains a status field indicating the
     * login status.
     */
    @GET("/xhrapp.php?app=api&m=checklogin")
    fun checkloginRequest(): Observable<XhrResponse>

    /**
     * Logs out the user.
     */
    @GET("/xhrapp.php?app=api&m=logout")
    fun logout(): Observable<XhrResponse>
}

/**
 * Checks whether the user is logged in by calling [AuthAPI.checkloginRequest].
 * @return whether the user is logged in
 */
fun AuthAPI.checkLogin(): Observable<Boolean> {
    return checkloginRequest().map {
        Log.v(LOG_TAG, "got checklogin response $it")
        it.status == 1
    }
}
