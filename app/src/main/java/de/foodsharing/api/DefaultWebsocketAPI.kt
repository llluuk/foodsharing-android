package de.foodsharing.api

import android.util.Log
import com.google.gson.Gson
import de.foodsharing.utils.BASE_URL
import de.foodsharing.utils.LOG_TAG
import de.foodsharing.utils.captureException
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.socket.client.IO
import io.socket.client.Socket
import io.socket.client.Socket.EVENT_CONNECT
import io.socket.client.Socket.EVENT_CONNECT_ERROR
import io.socket.client.Socket.EVENT_ERROR
import okhttp3.OkHttpClient
import org.json.JSONObject

class DefaultWebsocketAPI(httpClient: OkHttpClient, private val gson: Gson) : WebsocketAPI {

    private val io: Socket

    init {
        val opts = IO.Options().apply {
            path = "/chat/socket.io/"
            callFactory = httpClient
            webSocketFactory = httpClient
        }

        fun onError(vararg args: Any?) {
            val arg = args[0]
            when (arg) {
                is Throwable -> captureException(arg)
                else -> Log.wtf(LOG_TAG, "$arg")
            }
        }

        io = IO.socket(BASE_URL, opts).apply {
            on(EVENT_CONNECT) { emit("register") }
            on(EVENT_ERROR, ::onError)
            on(EVENT_CONNECT_ERROR, ::onError)
        }
    }

    override fun subscribe(): Observable<WebsocketAPI.Message> {
        if (!this.io.connected()) this.io.connect()
        return Observable.create { emitter ->

            fun onConv(vararg args: Any?) {
                val obj = args[0] as JSONObject
                val msg = gson.fromJson(obj.getString("o"), WebsocketAPI.ConversationMessage::class.java)
                emitter.onNext(msg)
            }

            fun onError(vararg args: Any?) {
                val arg = args[0]
                when (arg) {
                    is Throwable -> if (!emitter.isDisposed) emitter.onError(arg)
                }
            }

            io.apply {
                on("conv", ::onConv)
                on(EVENT_ERROR, ::onError)
                on(EVENT_CONNECT_ERROR, ::onError)
            }

            emitter.setDisposable(object : Disposable {

                var disposed = false

                override fun isDisposed(): Boolean {
                    return disposed
                }

                override fun dispose() {
                    disposed = true
                    io.apply {
                        off("conv", ::onConv)
                        off(EVENT_ERROR, ::onError)
                        off(EVENT_CONNECT_ERROR, ::onError)
                    }
                }
            })
        }
    }

    override fun close() {
        if (io.connected())
            io.disconnect()
    }
}