package de.foodsharing.api

/**
 * Class for Xhr responses that also contain data.
 * @param T the data type
 */
open class XhrDataResponse<T> : XhrResponse() {
    var data: T? = null
}
