package de.foodsharing.api

/**
 * Class for all Xhr responses that contain a status.
 */
open class XhrResponse {
    var status: Int = 0
}
