package de.foodsharing.api

import de.foodsharing.model.Basket
import de.foodsharing.model.FairSharePoint
import io.reactivex.Observable
import retrofit2.http.GET

interface MapAPI {

    data class MapResponse(
        val status: Int,
        val fairteiler: List<FairSharePoint>,
        val baskets: List<Basket>
    )

    @GET("/xhr.php?f=loadMarker&types[]=fairteiler&types[]=baskets")
    fun coordinates(): Observable<MapResponse>
}