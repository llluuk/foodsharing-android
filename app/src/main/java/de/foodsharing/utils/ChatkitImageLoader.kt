package de.foodsharing.utils

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.widget.ImageView
import com.stfalcon.chatkit.commons.ImageLoader
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.net.URL

/**
 * Utility class that loads images from a URL.
 */
open class ChatkitImageLoader(private val defaultImage: String? = null) : ImageLoader {

    override fun loadImage(imageView: ImageView, url: String?, payload: Any?) {
        createObservable(url)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                imageView.setImageBitmap(it.apply { setHasAlpha(true) })
            }, ::captureException)
    }

    /**
     * The observable is responsible for loading one image and optionally falling back to a default
     * image.
     */
    protected fun createObservable(url: String?): Observable<Bitmap> {
        var o = Observable.just(url)
            .map {
                BitmapFactory.decodeStream(URL(it).openConnection().getInputStream())
            }

        if (defaultImage != null) o = o.onErrorReturn {
            BitmapFactory.decodeStream(URL(defaultImage).openConnection().getInputStream())
        }

        return o
    }
}
