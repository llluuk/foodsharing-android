package de.foodsharing.utils

import android.support.design.widget.Snackbar
import android.view.View

object Utils {

    /**
     * Creates a [Snackbar] via [Snackbar.make] and sets its background color. The duration will
     * be [Snackbar.LENGTH_SHORT].
     *
     * @param view The view to find a parent from.
     * @param text The text to show. Can be formatted text.
     * @param backgroundColor The background color.
     *
     * @return the created Snackbar
     */
    fun showSnackBar(view: View, text: String, backgroundColor: Int): Snackbar =
        Snackbar.make(view, text, Snackbar.LENGTH_SHORT).withColor(backgroundColor)

    /**
     * Creates a [Snackbar] via [Snackbar.make] and sets its background color.
     *
     * @param view The view to find a parent from.
     * @param text The text to show. Can be formatted text.
     * @param backgroundColor The background color.
     * @param duration How long to display the message. Either [Snackbar.LENGTH_SHORT] or
     * [Snackbar.LENGTH_LONG].
     *
     * @return the created Snackbar
     */
    fun showSnackBar(view: View, text: String, backgroundColor: Int, duration: Int): Snackbar {
        return Snackbar.make(
            view,
            text,
            duration
        ).withColor(backgroundColor)
    }

    enum class PhotoType(val prefix: String) {
        NORMAL(""), CROP("crop_"), THUMB_CROP("thumb_crop_"),
        Q_130("130_q_"), MINI("mini_q_")
    }

    fun getUserPhotoURL(filename: String, type: PhotoType) =
        "$BASE_URL/images/${type.prefix}$filename"
}