package de.foodsharing.utils

import android.support.annotation.ColorInt
import android.support.annotation.LayoutRes
import android.support.design.widget.Snackbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * Sets the background color of a snackbar.
 * @param colorInt the background color
 *
 * @return this Snackbar
 */
fun Snackbar.withColor(@ColorInt colorInt: Int): Snackbar {
    view.setBackgroundColor(colorInt)
    return this
}

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}