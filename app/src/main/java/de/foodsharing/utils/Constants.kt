package de.foodsharing.utils

import de.foodsharing.BuildConfig

/** The base URL of the API */
val BASE_URL = BuildConfig.BASE_URL

val LOG_TAG = "foodsharing"

val DEFAULT_USER_PICTURE = "$BASE_URL/img/130_q_avatar.png"