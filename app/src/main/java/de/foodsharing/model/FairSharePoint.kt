package de.foodsharing.model

import com.google.gson.annotations.SerializedName

data class FairSharePoint(
    val id: Int,
    @SerializedName("bid")
    val regionId: Int,
    val lat: Double,
    val lon: Double
)
