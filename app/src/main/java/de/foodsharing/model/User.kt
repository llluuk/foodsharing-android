package de.foodsharing.model

/**
 * Represents a person that is participating in foodsharing.
 */
data class User(
    val id: Int,
    val email: String,
    val name: String,
    val photo: String?
)