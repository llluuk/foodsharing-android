package de.foodsharing.model

import java.util.Date

/**
 * A chat message that is part of a [ConversationDetail].
 */
data class Message(
    val id: Int,
    val fsId: Int,
    val fsName: String,
    val body: String,
    val time: Date
)