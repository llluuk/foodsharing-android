package de.foodsharing.model

import com.google.gson.annotations.SerializedName
import java.util.Date

/**
 * A basket is a collection of some food that a user wants to give away. People can upload a
 * description of that food, optionally including a picture. The basket contains informations about
 * where to pick up the food and how to contact the user.
 */
data class Basket(
    val id: Int,
    val description: String,
    val picture: String,
    @SerializedName("createdAt")
    val createdAt: Date,
    @SerializedName("updatedAt")
    val updatedAt: Date,
    @SerializedName("contactTypes")
    val contactTypes: Array<Int>,
    val until: Date,
    val lat: Double,
    val lon: Double,
    val creator: User
)