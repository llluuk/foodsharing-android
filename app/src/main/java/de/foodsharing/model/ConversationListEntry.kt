package de.foodsharing.model

import com.google.gson.annotations.SerializedName
import java.util.Date

/**
 * Metadata of a conversation that is included in a list of the user's conversations.
 */
data class ConversationListEntry(
    val id: Int,
    val last: String,
    @SerializedName("last_foodsaver_id")
    val lastFoodsaverID: Int,
    @SerializedName("last_message")
    val lastMessage: String?,
    @SerializedName("last_ts")
    val lastTS: Date,
    val name: String?,
    val unread: Int,
    @SerializedName("member")
    val members: List<User>
)