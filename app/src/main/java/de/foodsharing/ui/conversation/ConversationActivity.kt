package de.foodsharing.ui.conversation

import android.os.Bundle
import android.preference.PreferenceManager.getDefaultSharedPreferences
import android.view.MenuItem
import android.view.View.GONE
import android.view.View.VISIBLE
import com.stfalcon.chatkit.messages.MessagesListAdapter
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.model.ConversationDetail
import de.foodsharing.model.Message
import de.foodsharing.model.User
import de.foodsharing.utils.ChatkitImageLoader
import de.foodsharing.ui.base.BaseActivity
import de.foodsharing.utils.DEFAULT_USER_PICTURE
import kotlinx.android.synthetic.main.activity_conversation.*
import javax.inject.Inject

class ConversationActivity : BaseActivity(), ConversationContract.View, Injectable {

    @Inject
    lateinit var presenter: ConversationContract.Presenter

    private val members = mutableListOf<User>()

    private lateinit var adapter: MessagesListAdapter<ChatkitMessage>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.attach(this)
        setContentView(R.layout.activity_conversation)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val id = intent.getIntExtra("id", -1)
        supportActionBar?.title = "Conv: $id"

        message_input.setInputListener { message ->
            val trimmedMessage = message.toString().trim()
            trimmedMessage.isNotEmpty().also { isNotEmpty ->
                if (isNotEmpty) presenter.sendMessage(id, trimmedMessage)
            }
        }

        val userId = getDefaultSharedPreferences(this).getInt("userId", -1)
        adapter = MessagesListAdapter(userId.toString(),
            ChatkitImageLoader(DEFAULT_USER_PICTURE)
        )
        adapter.setDateHeadersFormatter(MessageDateFormatter(this))
        adapter.setLoadMoreListener { page, totalItemsCount -> presenter.fetchHistory(id, page, totalItemsCount) }
        recycler_view.setAdapter(adapter)

        message_input.visibility = GONE

        presenter.fetch(id)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }

    override fun display(conversation: ConversationDetail) {
        this.members.run { clear(); addAll(conversation.members) }
        if (members.size>2){
            if (conversation.name != null){ supportActionBar?.title = conversation.name
            supportActionBar?.subtitle = formatUserNames(members)}
            else supportActionBar?.title = formatUserNames(members)
        }else{
            supportActionBar?.title = formatUserNames(members)
        }
        adapter.clear()
        sortByDate(conversation.messages).forEach { addMessage(it) }

        progress_bar.visibility = GONE
        message_input.visibility = VISIBLE
    }

    override fun addMessage(message: Message) {
        findMessageAuthor(message)?.let { adapter.addToStart(ChatkitMessage(message, it), true) }
    }

    override fun addHistory(messages: List<Message>) {
        adapter.addToEnd(
            messages.map { m ->
                findMessageAuthor(m)?.let { ChatkitMessage(m, it) }
            }.filterNotNull(),
            false
        )
    }

    override fun clearInput() {
        message_input.inputEditText.text.clear()
    }

    override fun onDestroy() {
        presenter.unsubscribe()
        super.onDestroy()
    }

    private fun sortByDate(messages: List<Message>): List<Message> {
        return messages.sortedWith(compareBy { it.time.time })
    }

    /**
     * Formats the names of chat members for the title.
     */
    private fun formatUserNames(users: List<User>): String {
        val userId = getDefaultSharedPreferences(this).getInt("userId", -1)
        return buildString {
            users.filter { it.id != userId }
                .forEach { if (!isEmpty()) append(", "); append(it.name) }
        }
    }

    private fun findMessageAuthor(message: Message): User? =
        members.firstOrNull { it.id == message.fsId }
}
