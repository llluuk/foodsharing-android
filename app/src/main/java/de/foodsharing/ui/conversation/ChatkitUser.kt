package de.foodsharing.ui.conversation

import com.stfalcon.chatkit.commons.models.IUser
import de.foodsharing.model.User
import de.foodsharing.utils.DEFAULT_USER_PICTURE
import de.foodsharing.utils.Utils

class ChatkitUser(val user: User) : IUser {

    override fun getId() = user.id.toString()

    override fun getName() = user.name

    override fun getAvatar() =
        if (user.photo != null && user.photo.isNotEmpty())
            Utils.getUserPhotoURL(user.photo, Utils.PhotoType.Q_130)
        else DEFAULT_USER_PICTURE
}
