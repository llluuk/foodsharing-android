package de.foodsharing.ui.conversation

import android.util.Log
import de.foodsharing.api.ConversationsAPI
import de.foodsharing.api.WebsocketAPI
import de.foodsharing.model.Message
import de.foodsharing.ui.base.BasePresenter
import de.foodsharing.utils.LOG_TAG

class ConversationPresenter(private val conversations: ConversationsAPI, private val ws: WebsocketAPI) :
    BasePresenter<ConversationContract.View>(), ConversationContract.Presenter {

    override fun fetch(id: Int) {
        request(conversations.get(id)) { view?.display(it) }
        request(ws.subscribe()) { msg ->
            when (msg) {
                is WebsocketAPI.ConversationMessage -> {
                    if (msg.cid == id) {
                        Log.v(LOG_TAG, "recieved message in conv ${msg.fsName} said '${msg.body}'")
                        view?.addMessage(
                            Message(
                                msg.id,
                                msg.fsId,
                                msg.fsName,
                                msg.body,
                                msg.time
                            )
                        )
                    }
                }
            }
        }
    }

    override fun fetchHistory(id: Int, number: Int, offset: Int) {
        request(conversations.get(id, number, offset)) { view?.addHistory(it.messages) }
    }

    override fun sendMessage(cid: Int, body: String) {
        request(conversations.send(cid, body)) {
            view?.clearInput()
        }
    }
}
