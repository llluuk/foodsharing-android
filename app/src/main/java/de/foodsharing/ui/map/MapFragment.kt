package de.foodsharing.ui.map

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.model.Basket
import de.foodsharing.model.FairSharePoint
import de.foodsharing.ui.basket.BasketActivity
import de.foodsharing.utils.LOG_TAG
import org.osmdroid.bonuspack.clustering.RadiusMarkerClusterer
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.Marker
import javax.inject.Inject

class MapFragment : Fragment(), MapContract.View, Injectable {

    private val WRITE_REQUEST_CODE = 0

    @Inject
    lateinit var presenter: MapContract.Presenter

    private lateinit var mapView: MapView
    private lateinit var markerOverlay: RadiusMarkerClusterer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // request real-time permissions (api >= 23)
        requestPermissions(
            arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
            WRITE_REQUEST_CODE
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        presenter.attach(this)

        val view = inflater.inflate(R.layout.fragment_map, container, false)

        // set tile map provider
        mapView = view.findViewById(R.id.map)
        mapView.setTileSource(TileSourceFactory.DEFAULT_TILE_SOURCE)

        // add default controls
        mapView.setBuiltInZoomControls(true)
        mapView.setMultiTouchControls(true)

        // set initial zoom and point
        val mapController = mapView.controller
        mapController.setZoom(6.0)
        mapController.setCenter(GeoPoint(51.0, 9.0))

        markerOverlay = RadiusMarkerClusterer(context)
        mapView.overlays.add(markerOverlay)

        presenter.fetch()

        return view
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            WRITE_REQUEST_CODE -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                // TODO
            } else {
                // Permission denied.
                // TODO
            }
        }
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun setMarkers(fairSharePoints: List<FairSharePoint>, baskets: List<Basket>) {
        markerOverlay.items.clear()

        // add fair-share-point markers
        val fspIcon = ContextCompat.getDrawable(context!!, R.drawable.marker_fairteiler)
        fairSharePoints.map { fsp ->
            val marker = Marker(mapView)
            marker.id = fsp.id.toString()
            marker.position = GeoPoint(fsp.lat, fsp.lon)
            marker.icon = fspIcon
            marker.relatedObject = fsp
            marker.setOnMarkerClickListener { m, _ ->
                val fairsp = m.relatedObject as FairSharePoint
                Log.v(LOG_TAG, "$fairsp")
                true
            }
            marker
        }.forEach { markerOverlay.add(it) }

        val basketIcon = ContextCompat.getDrawable(context!!, R.drawable.marker_basket)
        baskets.map { b ->
            val marker = Marker(mapView)
            marker.id = b.id.toString()
            marker.position = GeoPoint(b.lat, b.lon)
            marker.icon = basketIcon
            marker.relatedObject = b
            marker.setOnMarkerClickListener { m, _ ->
                val basket = m.relatedObject as Basket
                Log.v(LOG_TAG, "$basket")

                // show basket details in an activity
                val intent = Intent(context, BasketActivity::class.java)
                intent.putExtra("id", basket.id)
                startActivity(intent)
                activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
                true
            }
            marker
        }.forEach { markerOverlay.add(it) }

        markerOverlay.invalidate()
        mapView.invalidate()
    }

    override fun onDestroy() {
        presenter.unsubscribe()
        super.onDestroy()
    }
}
