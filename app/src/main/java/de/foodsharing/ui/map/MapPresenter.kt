package de.foodsharing.ui.map

import de.foodsharing.api.MapAPI
import de.foodsharing.ui.base.BasePresenter

class MapPresenter(private val map: MapAPI) : BasePresenter<MapContract.View>(), MapContract.Presenter {

    override fun fetch() {
        request(map.coordinates()) { view?.setMarkers(it.fairteiler, it.baskets) }
    }
}