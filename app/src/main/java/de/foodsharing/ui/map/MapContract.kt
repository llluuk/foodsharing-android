package de.foodsharing.ui.map

import de.foodsharing.model.Basket
import de.foodsharing.model.FairSharePoint
import de.foodsharing.ui.base.BaseContract

class MapContract {

    interface View : BaseContract.View {
        fun setMarkers(fairSharePoints: List<FairSharePoint>, baskets: List<Basket>)
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun fetch()
    }
}