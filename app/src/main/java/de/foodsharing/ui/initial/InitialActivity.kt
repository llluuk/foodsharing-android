package de.foodsharing.ui.initial

import android.content.Intent
import android.os.Bundle
import de.foodsharing.api.AuthAPI
import de.foodsharing.api.checkLogin
import de.foodsharing.di.Injectable
import de.foodsharing.ui.base.BaseActivity
import de.foodsharing.ui.login.LoginActivity
import de.foodsharing.ui.main.MainActivity
import io.reactivex.Observable.just
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class InitialActivity : BaseActivity(), Injectable {

    @Inject
    lateinit var auth: AuthAPI

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        auth.checkLogin()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext(just(false))
            .subscribe { isLoggedIn ->
                val intent = Intent(
                    this, if (isLoggedIn) {
                        MainActivity::class.java
                    } else {
                        LoginActivity::class.java
                    }
                )
                startActivity(intent)
                finish()
            }
    }
}
