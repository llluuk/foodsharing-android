package de.foodsharing.ui.conversations

import de.foodsharing.model.ConversationListEntry

interface ConversationActionListener {
    fun onViewConversation(conversation: ConversationListEntry)
}