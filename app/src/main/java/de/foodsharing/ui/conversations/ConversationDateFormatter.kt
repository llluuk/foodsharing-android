package de.foodsharing.ui.conversations

import android.content.Context
import com.stfalcon.chatkit.utils.DateFormatter
import de.foodsharing.R
import java.util.Date

/**
 * Formats the date that is displayed above chat messages.
 */
class ConversationDateFormatter(val context: Context) : DateFormatter.Formatter {

    override fun format(date: Date): String {
        if (DateFormatter.isToday(date)) {
            return DateFormatter.format(date, DateFormatter.Template.TIME)
        } else if (DateFormatter.isYesterday(date)) {
            return context.getString(R.string.date_yesterday)
        } else if (DateFormatter.isCurrentYear(date)) {
            return DateFormatter.format(date, DateFormatter.Template.STRING_DAY_MONTH)
        } else {
            return DateFormatter.format(date, DateFormatter.Template.STRING_DAY_MONTH_YEAR)
        }
    }
}