package de.foodsharing.ui.conversations

import com.stfalcon.chatkit.commons.models.IDialog
import de.foodsharing.model.ConversationListEntry
import de.foodsharing.ui.conversation.ChatkitMessage
import de.foodsharing.ui.conversation.ChatkitUser
import de.foodsharing.utils.BASE_URL
import de.foodsharing.utils.DEFAULT_USER_PICTURE
import de.foodsharing.utils.Utils
import kotlin.math.min

class ChatkitConversation(
    val conversation: ConversationListEntry,
    var message: ChatkitMessage,
    val currentUserID: Int,
    var unread: Int = conversation.unread
) :
    IDialog<ChatkitMessage> {

    override fun getId() = conversation.id.toString()

    override fun getDialogPhoto(): String {
        // use up to 4 user pictures as the group picture, exclude current user
        val num = min(conversation.members.size - 1, 4)

        if (num < 1) return ""
        else return conversation.members
            .filter { it.id != currentUserID }
            .shuffled()
            .subList(0, num)
            .joinToString("|") {
                if (it.photo == null) DEFAULT_USER_PICTURE
                else Utils.getUserPhotoURL(it.photo, Utils.PhotoType.Q_130)
            }
    }

    override fun getDialogName(): String {
        //return the names of participants if conv. name == null, exclude user
        val num = conversation.members.size - 1
        if (conversation.name == null) return conversation.members
                .filter { it.id != currentUserID }
                .shuffled()
                .subList(0, num)
                .joinToString("|") {
                    it.name }
        else return conversation.name
    }

    override fun getUsers(): List<ChatkitUser> = conversation.members.map { ChatkitUser(it) }

    override fun getLastMessage(): ChatkitMessage = message

    override fun setLastMessage(message: ChatkitMessage) {
        this.message = message
    }

    override fun getUnreadCount() = unread
}