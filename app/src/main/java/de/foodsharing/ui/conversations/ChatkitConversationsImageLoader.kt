package de.foodsharing.ui.conversations

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.widget.ImageView
import de.foodsharing.utils.ChatkitImageLoader
import de.foodsharing.utils.captureException
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Utility class that loads multiple images from URLS and combines them into one image.
 */
class ChatkitConversationsImageLoader(defaultImage: String? = null) :
    ChatkitImageLoader(defaultImage) {

    private val imageMargin = 4

    override fun loadImage(imageView: ImageView, url: String?, payload: Any?) {
        val urls = url?.split("|")

        if (urls != null && urls.size > 1) {
            val os = urls.map { createObservable(it) }

            Observable.merge(os).toList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    imageView.setImageBitmap(
                        combineImages(it).apply {
                            setHasAlpha(true)
                        }
                    )
                }, ::captureException)
        } else super.loadImage(imageView, url, payload)
    }

    /**
     * Combines up to four images into one.
     */
    private fun combineImages(images: List<Bitmap>): Bitmap {
        val result = Bitmap.createBitmap(
            images[0].getWidth() * 2 + imageMargin,
            images[0].getHeight() * 2 + imageMargin,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(result)
        val paint = Paint()
        for (i in images.indices) {
            canvas.drawBitmap(
                images[i],
                (images[i].getWidth().toFloat() + imageMargin) * (i % 2),
                (images[i].getHeight().toFloat() + imageMargin) * (i / 2),
                paint
            )
        }
        return result
    }
}