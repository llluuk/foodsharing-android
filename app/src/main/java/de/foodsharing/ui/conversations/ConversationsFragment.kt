package de.foodsharing.ui.conversations

import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.stfalcon.chatkit.dialogs.DialogsListAdapter
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.model.ConversationListEntry
import de.foodsharing.model.Message
import de.foodsharing.model.User
import de.foodsharing.ui.conversation.ChatkitMessage
import de.foodsharing.ui.conversation.ConversationActivity
import de.foodsharing.utils.DEFAULT_USER_PICTURE
import kotlinx.android.synthetic.main.activity_conversation.*
import kotlinx.android.synthetic.main.fragment_conversation_list.view.*
import javax.inject.Inject

class ConversationsFragment : Fragment(), ConversationsContract.View,
    DialogsListAdapter.OnDialogClickListener<ChatkitConversation>,
    Injectable {

    @Inject
    lateinit var presenter: ConversationsContract.Presenter

    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var adapter: DialogsListAdapter<ChatkitConversation>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        presenter.attach(this)

        val view = inflater.inflate(R.layout.fragment_conversation_list, container, false)

        layoutManager = LinearLayoutManager(activity)

        view.recycler_view.layoutManager = layoutManager
        adapter = DialogsListAdapter(ChatkitConversationsImageLoader(DEFAULT_USER_PICTURE))
        adapter.setOnDialogClickListener(this)
        adapter.setDatesFormatter(ConversationDateFormatter(context!!))
        view.recycler_view.setAdapter(adapter)

        presenter.fetch()

        return view
    }

    override fun display(conversations: List<ConversationListEntry>) {
        val userId = PreferenceManager.getDefaultSharedPreferences(context).getInt("userId", -1)

        adapter.clear()
        adapter.addItems(
            conversations
                .filter { c -> c.lastMessage != null }
                .mapNotNull { c ->
                    findUserInConversation(c, c.lastFoodsaverID)?.let { u ->
                        val msg = Message(-1, u.id, u.name, c.lastMessage!!, c.lastTS)
                        ChatkitConversation(c, ChatkitMessage(msg, u), userId)
                    }
                }
        )

        progress_bar.visibility = View.GONE
    }

    override fun updateLatestMessage(cid: Int, message: Message) {
        adapter.getItemById(cid.toString())?.let { c ->
            findUserInConversation(c.conversation, message.fsId)?.let { u ->
                c.lastMessage = ChatkitMessage(message, u)
                c.unread += 1

                adapter.moveItem(adapter.getDialogPosition(c), 0)
                adapter.notifyDataSetChanged()
                layoutManager.scrollToPosition(0)
            }
        }
    }

    /**
     * Finds and returns the user with the specified id in the conversation, or null if not found.
     */
    private fun findUserInConversation(conversation: ConversationListEntry, id: Int): User? {
        return conversation.members.firstOrNull { it.id == id }
    }

    override fun onDialogClick(conversation: ChatkitConversation) {
        // reset unread messages
        conversation.unread = 0
        adapter.notifyItemChanged(adapter.getDialogPosition(conversation))

        // show conversation
        val intent = Intent(context, ConversationActivity::class.java)
        intent.putExtra("id", conversation.conversation.id)
        startActivity(intent)
        activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
    }
}