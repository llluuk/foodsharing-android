package de.foodsharing.ui.base

import de.foodsharing.ui.base.BaseContract.Presenter
import de.foodsharing.utils.captureException
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

abstract class BasePresenter<View> : Presenter<View> {

    protected var view: View? = null
    private val subscriptions = CompositeDisposable()

    fun <T> request(o: Observable<T>, s: (T) -> Unit) {
        request(o, s, { })
    }

    fun <T> request(o: Observable<T>, s: (T) -> Unit, e: (Throwable) -> Unit) {
        subscriptions.add(
            o
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s, {
                    captureException(it)
                    e(it)
                })
        )
    }

    override fun unsubscribe() {
        subscriptions.clear()
    }

    override fun attach(view: View) {
        this.view = view
    }
}