package de.foodsharing.ui.basket

import android.graphics.Bitmap
import de.foodsharing.model.Basket
import de.foodsharing.ui.base.BaseContract

class BasketContract {

    interface View : BaseContract.View {
        fun display(basket: Basket)
        fun displayPicture(picture: Bitmap)
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun fetch(id: Int)
        fun loadPicture(file: String)
    }
}