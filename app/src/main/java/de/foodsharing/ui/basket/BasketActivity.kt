package de.foodsharing.ui.basket

import android.graphics.Bitmap
import android.os.Bundle
import android.view.MenuItem
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ImageView
import android.widget.TextView
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.model.Basket
import de.foodsharing.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_basket.*
import java.text.SimpleDateFormat
import java.util.Locale
import javax.inject.Inject

class BasketActivity : BaseActivity(), BasketContract.View, Injectable {

    private val dateFormat = SimpleDateFormat("EEE, d MMM yyyy HH:mm", Locale.getDefault())

    @Inject
    lateinit var presenter: BasketContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.attach(this)
        setContentView(R.layout.activity_basket)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val id = intent.getIntExtra("id", -1)
        supportActionBar?.title = "Basket: #$id"

        presenter.fetch(id)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }

    override fun onDestroy() {
        presenter.unsubscribe()
        super.onDestroy()
    }

    override fun display(basket: Basket) {
        findViewById<TextView>(R.id.basket_created_at).text = dateFormat.format(basket.createdAt)
        findViewById<TextView>(R.id.basket_valid_until).text = dateFormat.format(basket.until)
        findViewById<TextView>(R.id.basket_description).text = basket.description

        basket_content_view.visibility = VISIBLE
        progress_bar.visibility = GONE

        if (basket.picture.isNotEmpty())
            presenter.loadPicture(basket.picture)
    }

    override fun displayPicture(picture: Bitmap) {
        findViewById<ImageView>(R.id.basket_picture).setImageBitmap(picture)
    }
}
