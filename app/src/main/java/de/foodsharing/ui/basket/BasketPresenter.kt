package de.foodsharing.ui.basket

import android.graphics.BitmapFactory
import de.foodsharing.api.BasketAPI
import de.foodsharing.ui.base.BasePresenter
import de.foodsharing.utils.BASE_URL
import de.foodsharing.utils.captureException
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.net.URL

class BasketPresenter(private val baskets: BasketAPI) :
    BasePresenter<BasketContract.View>(), BasketContract.Presenter {

    override fun fetch(id: Int) {
        request(baskets.get(id)) { view?.display(it.basket!!) }
    }

    override fun loadPicture(file: String) {
        Observable.just(file)
            .map {
                BitmapFactory.decodeStream(URL("$BASE_URL/images/basket/$file").openConnection().getInputStream())
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                it.setHasAlpha(true)
                view?.displayPicture(it)
            }, ::captureException)
    }
}
