package de.foodsharing.ui.newbasket

import de.foodsharing.api.BasketAPI
import de.foodsharing.ui.base.BasePresenter

class NewBasketPresenter(private val baskets: BasketAPI) :
    BasePresenter<NewBasketContract.View>(), NewBasketContract.Presenter {

    private val contactTypeMessage = 1
    private val contactTypePhone = 2

    override fun publish(
        description: String,
        phone: String?,
        mobile: String?,
        contactByMessage: Boolean,
        weight: Float,
        lifetime: Int,
        location: Pair<Double, Double>?
    ) {
        val contactTypes = ArrayList<Int>()

        if (phone != null && mobile != null) contactTypes.add(contactTypePhone)
        if (contactByMessage) contactTypes.add(contactTypeMessage)

        request(
            baskets.create(
                description,
                contactTypes.toTypedArray(),
                phone,
                mobile,
                weight,
                lifetime,
                location?.first,
                location?.second
            )
        ) { view?.display(it.basket!!) }
    }
}