package de.foodsharing.ui.newbasket

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View.GONE
import android.view.View.VISIBLE
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.model.Basket
import de.foodsharing.ui.base.BaseActivity
import de.foodsharing.ui.basket.BasketActivity
import kotlinx.android.synthetic.main.activity_new_basket.*
import javax.inject.Inject

class NewBasketActivity : BaseActivity(), NewBasketContract.View, Injectable {

    @Inject
    lateinit var presenter: NewBasketContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.attach(this)
        setContentView(R.layout.activity_new_basket)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.basket_new_title)

        basket_checkbox_phone.setOnCheckedChangeListener { _, checked ->
            basket_phone_input.visibility = if (checked) VISIBLE else GONE
        }
        basket_publish_button.setOnClickListener { view ->
            TODO("not implemented")
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }

    override fun onDestroy() {
        presenter.unsubscribe()
        super.onDestroy()
    }

    override fun display(basket: Basket) {
        // show details of created basket in an activity
        val intent = Intent(this, BasketActivity::class.java)
        intent.putExtra("id", basket.id)
        startActivity(intent)
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
    }
}
