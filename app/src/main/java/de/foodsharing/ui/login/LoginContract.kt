package de.foodsharing.ui.login

import de.foodsharing.ui.base.BaseContract

class LoginContract {

    interface View : BaseContract.View {
        fun showProgress(show: Boolean)
        fun loginResult(result: Boolean, userId: Int?)
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun checkLogin()
        fun login(user: String, password: String)
    }
}