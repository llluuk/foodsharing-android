package de.foodsharing.ui.login

import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager.getDefaultSharedPreferences
import android.view.View
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.ui.base.BaseActivity
import de.foodsharing.ui.main.MainActivity
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

class LoginActivity : BaseActivity(), LoginContract.View,
    BaseActivity.OnConnectionAvailableListener, Injectable {

    @Inject
    lateinit var presenter: LoginContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        rootLayoutID = R.id.root
        onConnectedListener = this
        presenter.attach(this)

        login_button.setOnClickListener {
            if (isConnected) {
                showProgress(true)
                presenter.login(user_field.text.toString(), password_field.text.toString())
            }
        }

        presenter.checkLogin()
    }

    override fun showProgress(show: Boolean) {
        progress_bar.visibility = if (show) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    override fun loginResult(result: Boolean, userId: Int?) {
        showProgress(false)
        if (result) {
            if (userId != null) getDefaultSharedPreferences(this).edit()
                .putInt("userId", userId).apply()
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        } else {
            showErrorMessage(getString(R.string.invalid_password))
        }
    }

    override fun showErrorMessage(error: String) {
        showProgress(false)
        showMessage(error)
    }

    override fun onConnectionAvailable() {
    }

    override fun onDestroy() {
        presenter.unsubscribe()
        super.onDestroy()
    }
}
