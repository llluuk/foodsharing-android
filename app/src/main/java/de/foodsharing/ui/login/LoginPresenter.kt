package de.foodsharing.ui.login

import android.util.Log
import de.foodsharing.api.AuthAPI
import de.foodsharing.api.LoginRequest
import de.foodsharing.api.checkLogin
import de.foodsharing.ui.base.BasePresenter
import de.foodsharing.utils.LOG_TAG

class LoginPresenter(private val auth: AuthAPI) : BasePresenter<LoginContract.View>(),
    LoginContract.Presenter {

    override fun checkLogin() {
        request(auth.checkLogin()) { isLoggedIn ->
            if (isLoggedIn) {
                view?.loginResult(true, null)
            }
        }
    }

    override fun login(user: String, password: String) {
        request(auth.login(LoginRequest(user, password, true)), { res ->
            Log.v(LOG_TAG, "Logged in as ${res.id} ${res.name}")
            view?.loginResult(true, res.id)
        }, { error ->
            view?.showErrorMessage(error.localizedMessage)
        })
    }
}