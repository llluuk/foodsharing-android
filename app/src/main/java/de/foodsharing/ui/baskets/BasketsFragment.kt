package de.foodsharing.ui.baskets

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.model.Basket
import de.foodsharing.ui.basket.BasketActivity
import de.foodsharing.ui.newbasket.NewBasketActivity
import kotlinx.android.synthetic.main.fragment_baskets.*
import kotlinx.android.synthetic.main.fragment_baskets.view.*
import javax.inject.Inject

class BasketsFragment : Fragment(), BasketsContract.View, BasketActionListener, Injectable {

    @Inject
    lateinit var presenter: BasketsContract.Presenter

    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var adapter: BasketListAdapter
    private val baskets = ArrayList<Basket>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        presenter.attach(this)

        val view = inflater.inflate(R.layout.fragment_baskets, container, false)

        layoutManager = LinearLayoutManager(activity)
        view.recycler_view.layoutManager = layoutManager
        adapter = BasketListAdapter(baskets, this)
        view.recycler_view.adapter = adapter

        val divider = DividerItemDecoration(context, layoutManager.orientation)
        context
            ?.let { ContextCompat.getDrawable(it, R.drawable.abc_list_divider_material) }
            ?.let { divider.setDrawable(it) }
        view.recycler_view.addItemDecoration(divider)

        view.add_basket_button.setOnClickListener {
            val intent = Intent(context, NewBasketActivity::class.java)
            startActivity(intent)
            activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
        }

        presenter.fetch()

        return view
    }

    override fun display(baskets: List<Basket>) {
        this.baskets.clear()
        this.baskets.addAll(baskets)
        adapter.notifyDataSetChanged()

        progress_bar.visibility = View.GONE

        if (baskets.isEmpty()) {
            no_baskets_label.visibility = View.VISIBLE
        } else {
            recycler_view.visibility = View.VISIBLE
        }
    }

    override fun onViewBasket(basket: Basket) {
        // show basket details in an activity
        val intent = Intent(context, BasketActivity::class.java)
        intent.putExtra("id", basket.id)
        startActivity(intent)
        activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
    }
}
