package de.foodsharing.ui.baskets

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import de.foodsharing.R
import de.foodsharing.model.Basket
import de.foodsharing.utils.inflate
import kotlinx.android.synthetic.main.item_basket.view.*

class BasketListAdapter(
    val baskets: List<Basket>,
    private val listener: BasketActionListener
) : RecyclerView.Adapter<BasketListAdapter.BasketHolder>() {

    override fun getItemCount(): Int = baskets.size

    override fun onBindViewHolder(holder: BasketListAdapter.BasketHolder, position: Int) {
        holder.bind(baskets[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewInt: Int): BasketHolder {
        return BasketListAdapter.BasketHolder(
            parent.inflate(
                R.layout.item_basket,
                false
            ), listener
        )
    }

    class BasketHolder(val view: View, private val listener: BasketActionListener) :
        RecyclerView.ViewHolder(view), View.OnClickListener {

        private var basket: Basket? = null

        init {
            view.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            basket?.let {
                listener.onViewBasket(it)
            }
        }

        fun bind(basket: Basket) {
            this.basket = basket
            view.item_id.text = basket.id?.toString()
            view.item_creation_date.text = basket.createdAt?.toString()
            view.item_description.text = basket.description
        }
    }
}