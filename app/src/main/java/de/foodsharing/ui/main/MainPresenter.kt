package de.foodsharing.ui.main

import com.franmontiel.persistentcookiejar.ClearableCookieJar
import de.foodsharing.api.AuthAPI
import de.foodsharing.api.WebsocketAPI
import de.foodsharing.ui.base.BasePresenter

class MainPresenter(
    private val auth: AuthAPI,
    private val ws: WebsocketAPI,
    private val cookieJar: ClearableCookieJar
) : BasePresenter<MainContract.View>(),
    MainContract.Presenter {

    override fun logout() {
        request(auth.logout()) {}
        ws.close()
        cookieJar.clear()
    }
}