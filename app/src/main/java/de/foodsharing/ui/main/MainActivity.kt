package de.foodsharing.ui.main

import android.content.Intent
import android.os.Bundle
import android.support.v4.view.GravityCompat
import android.view.MenuItem
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.ui.base.BaseActivity
import de.foodsharing.ui.login.LoginActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : BaseActivity(), MainContract.View, Injectable {

    @Inject
    lateinit var presenter: MainContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_drawer) // TODO: where was ic_menu?
        }

        val pagerAdapter = MainPagerAdapter(supportFragmentManager)

        main_pager.adapter = pagerAdapter
        main_tab_layout.setupWithViewPager(main_pager)

        for (position in 0 until pagerAdapter.count) {
            main_tab_layout.getTabAt(position)?.apply {
                setIcon(pagerAdapter.getPageIcon(position))
            }
        }

        main_tab_layout.getTabAt(0)?.select()

        // set actions for menu items in the drawer
        nav_view.setNavigationItemSelectedListener { menuItem ->
            // set item as selected to persist highlight
            menuItem.isChecked = true
            // close drawer when item is tapped
            drawer_layout.closeDrawers()

            when (menuItem.itemId) {
                R.id.nav_account -> {
                    // TODO
                }
                R.id.nav_logout -> logout()
            }

            true
        }

        presenter.attach(this)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                drawer_layout.openDrawer(GravityCompat.START)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    /**
     * Logs out the user and switches to the LoginActivity. This is called when the logout button
     * in the drawer is selected.
     */
    private fun logout() {
        presenter.logout()

        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }

    override fun onDestroy() {
        presenter.unsubscribe()
        super.onDestroy()
    }
}
