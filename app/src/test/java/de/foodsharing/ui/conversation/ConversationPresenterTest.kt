package de.foodsharing.ui.conversation

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import de.foodsharing.api.ConversationsAPI
import de.foodsharing.api.WebsocketAPI
import de.foodsharing.model.ConversationDetail
import de.foodsharing.model.Message
import de.foodsharing.model.User
import de.foodsharing.test.configureTestSchedulers
import io.reactivex.Observable
import io.reactivex.Observable.just
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import java.util.Date

class ConversationPresenterTest {

    lateinit var presenter: ConversationPresenter

    @Mock
    lateinit var view: ConversationContract.View

    @Mock
    lateinit var conversations: ConversationsAPI

    @Mock
    lateinit var ws: WebsocketAPI

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        configureTestSchedulers()
        presenter = ConversationPresenter(conversations, ws)
        presenter.attach(view)
    }

    @Test
    fun `fetch conversation`() {
        val userId = 123
        val fsName = "Foo"
        val conversationId = 1314
        val name = "convName"
        val members = listOf(User(userId, "foo@foo.com", fsName, null))
        val messages = listOf(Message(10, userId, fsName, "a nice message", Date()))
        whenever(conversations.get(conversationId)) doReturn just(ConversationDetail(members, messages,name))
        whenever(ws.subscribe()) doReturn Observable.create { }
        presenter.fetch(conversationId)
        verify(view).display(ConversationDetail(members, messages,name))
    }

    @Test
    fun `fetch subscribes to websocket`() {
        val userId = 123
        val fsName = "Foo"
        val conversationId = 1314
        val name = "convN"
        val date = Date()
        val members = listOf(User(userId, "foo@foo.com", fsName, null))
        val messages = listOf(Message(10, userId, fsName, "a nice message", date))
        val receiveMessages = listOf(
            WebsocketAPI.ConversationMessage(
                10,
                conversationId,
                userId,
                fsName,
                null,
                "a nice new message",
                date
            ),
            WebsocketAPI.ConversationMessage(
                11,
                conversationId,
                userId,
                fsName,
                null,
                "another nice new message",
                date
            ),
            // Should ignore this one from another conversation id
            WebsocketAPI.ConversationMessage(
                12,
                conversationId + 1,
                userId,
                fsName,
                null,
                "ignored",
                date
            )
        )
        whenever(conversations.get(conversationId)) doReturn just(
            ConversationDetail(
                members,
                messages,
                name
                )
        )
        whenever(ws.subscribe()) doReturn Observable.create { receiveMessages.forEach(it::onNext) }
        presenter.fetch(conversationId)
        verify(view).display(ConversationDetail(members, messages, name))
        verify(view).addMessage(Message(10, userId, fsName, "a nice new message", date))
        verify(view).addMessage(Message(11, userId, fsName, "another nice new message", date))
    }

    @Test
    fun `clears input after sending message`() {
        whenever(conversations.send(anyInt(), anyString())) doReturn just(true)
        presenter.sendMessage(123, "yay")
        verify(view).clearInput()
    }

    @After
    fun after() {
        verifyNoMoreInteractions(view)
    }
}